use std::{
    fs,
    io::{BufWriter, Write},
    process::{Command, Output, Stdio},
};

use camino::Utf8Path;
use itertools::Itertools;
use proc_macro2::{Ident, TokenStream};
use quote::quote;
use syn::Error;
use walkdir::WalkDir;

pub fn entrypoint(scene_mod_dir: &Utf8Path, codegen_file: &Utf8Path) {
    let mut codegen_buf = create_codegen_buf();

    let (game_scenes, first_struct) = scan_files(scene_mod_dir, codegen_file);

    let variants = get_game_scene_enum_variants(&game_scenes[..]);
    let from_impls = get_game_scene_from_impls(&game_scenes[..]);

    let enum_impls = get_enum_game_scene_impl(&game_scenes[..]);

    gen_enum_and_impls(&mut codegen_buf, &variants, &first_struct, &from_impls, &enum_impls);

    let mut codegen_buf = codegen_buf.into_inner().unwrap();
    format_code(&mut codegen_buf);

    fs::write(codegen_file, codegen_buf)
        .unwrap_or_else(|e| panic!("unable to write formatted code to file {codegen_file}: {e}"));
}

fn create_codegen_buf() -> BufWriter<Vec<u8>> {
    BufWriter::new(
        quote! {
            use std::future::Future;

            use anyhow::Result;
            use crossterm::event::Event;
            use ratatui::prelude::*;

            use super::*;

            pub trait GameScene<'a>
            where
                &'a Self: Widget, Self: 'a
            {
                fn handle_event(&'a mut self, event: &Event) -> impl Future<Output=Result<GameSceneTransition>>;
            }

            pub enum GameSceneTransition {
                Continue,
                Pop,
                Push(GameSceneType)
            }
        }
        .to_string()
        .into_bytes(),
    )
}

fn scan_files(scene_mod_dir: &Utf8Path, codegen_file: &Utf8Path) -> (Vec<Ident>, Ident) {
    let mut first = None;

    let structs = WalkDir::new(scene_mod_dir)
        .follow_links(true)
        .into_iter()
        .filter_map(|entry| {
            let entry = entry.expect("unable to read directory entry");
            let is_file = entry.file_type().is_file();
            if is_file && entry.file_name().as_encoded_bytes().ends_with(b".rs") && (entry.path() != codegen_file) {
                Some(
                    Utf8Path::from_path(entry.path())
                        .expect("only UTF-8 paths are supported")
                        .to_path_buf(),
                )
            } else {
                None
            }
        })
        .flat_map(|file| {
            let (structs, first_) = get_game_scene_structs(file);

            match (&mut first, first_) {
                (Some(f), Some(f_)) => panic!(
                    "{}",
                    Error::new_spanned(f_, format!("GameScope(first) defined multiple times ({f})"))
                        .into_compile_error()
                ),
                (None, Some(f)) => first = Some(f),
                _ => (),
            }

            structs
        })
        .collect();

    let Some(first) = first else {
        panic!("GameScene(first) not defined");
    };

    (structs, first)
}

fn get_game_scene_structs(source_file: impl AsRef<Utf8Path>) -> (Vec<Ident>, Option<Ident>) {
    let mut first = None;

    let structs = syn::parse_file(
        &fs::read_to_string(source_file.as_ref())
            .unwrap_or_else(|e| panic!("unable to read source file {}: {e}", source_file.as_ref())),
    )
    .unwrap_or_else(|e| {
        panic!("{}", e.into_compile_error());
    })
    .items
    .into_iter()
    .filter_map(|item| match item {
        syn::Item::Struct(struct_) => Some(struct_),
        _ => None,
    })
    .filter_map(|struct_| {
        use syn::{Meta::*, Path};
        let is_game_scene_attr = |path: &Path| path.segments.first().unwrap().ident == "GameScene";

        struct_
            .attrs
            .iter()
            .any(|attr| match &attr.meta {
                NameValue(_) => false,
                Path(path) => is_game_scene_attr(path),
                List(list) => {
                    if is_game_scene_attr(&list.path) {
                        if let Some(ident) = &first {
                            panic!(
                                "{}",
                                Error::new_spanned(list, format!("GameScene(first) defined multiple times ({ident})"))
                                    .into_compile_error()
                            )
                        }
                        // We validate in the derive crate, so if there's arguments then it has "first"
                        first = Some(struct_.ident.clone());
                        true
                    } else {
                        false
                    }
                }
            })
            .then_some(struct_.ident)
    })
    .collect_vec();

    (structs, first)
}

fn get_game_scene_enum_variants(game_scenes: &[Ident]) -> Vec<TokenStream> {
    game_scenes
        .iter()
        .map(|struct_name| {
            quote! {
                #struct_name(#struct_name)
            }
        })
        .collect()
}

fn get_game_scene_from_impls(game_scenes: &[Ident]) -> Vec<TokenStream> {
    game_scenes
        .iter()
        .map(|struct_name| {
            quote! {
                impl From<#struct_name> for GameSceneType {
                    fn from(scene: #struct_name) -> GameSceneType {
                        GameSceneType::#struct_name(scene)
                    }
                }
            }
        })
        .collect()
}

fn get_enum_game_scene_impl(game_scenes: &[Ident]) -> TokenStream {
    let (variants_event, variants_widget): (Vec<_>, Vec<_>) = game_scenes
        .iter()
        .map(|struct_name| {
            (
                quote! {
                    GameSceneType::#struct_name(s) => s.handle_event(event).await
                },
                quote! {
                    GameSceneType::#struct_name(s) => s.render(area, buf)
                },
            )
        })
        .multiunzip();
    quote! {
        impl Widget for &GameSceneType {
            fn render(self, area: Rect, buf: &mut Buffer)
            where
                Self: Sized,
            {
                match self {
                    #(#variants_widget),*
                }
            }
        }

        impl<'a> GameScene<'a> for GameSceneType {
            async fn handle_event(&'a mut self, event: &Event) -> Result<GameSceneTransition> {
                match self {
                    #(#variants_event),*
                }
            }
        }
    }
}

fn gen_enum_and_impls(
    codegen_buf: &mut BufWriter<Vec<u8>>,
    variants: &[TokenStream],
    first_struct: &Ident,
    from_impls: &[TokenStream],
    enum_impls: &TokenStream,
) {
    codegen_buf
        .write_all(
            quote! {
                pub enum GameSceneType {
                    #(#variants),*
                }
                impl GameSceneType {
                    pub async fn new() -> Result<Self> {
                        Ok(Self::#first_struct(#first_struct::new().await?))
                    }
                }

                #enum_impls

                #(#from_impls)*
            }
            .to_string()
            .as_bytes(),
        )
        .unwrap();
}

fn format_code(codegen_buf: &mut Vec<u8>) {
    let mut cmd_handle = Command::new("rustfmt")
        .args(["--edition", "2021"])
        .stdin(Stdio::piped())
        .stdout(Stdio::piped())
        .stderr(Stdio::piped())
        .spawn()
        .unwrap_or_else(|e| panic!("unable to run rustfmt command: {e}"));

    cmd_handle
        .stdin
        .as_mut()
        .unwrap()
        .write_all(codegen_buf)
        .expect("unable to pipe code to rustfmt");

    let Output { status, stderr, stdout } = cmd_handle
        .wait_with_output()
        .expect("unable to wait on rustfmt command");

    if !status.success() {
        panic!("rustfmt {}", String::from_utf8_lossy(&stderr[..]));
    }

    *codegen_buf = stdout;
}
