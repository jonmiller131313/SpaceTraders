use proc_macro::TokenStream;

use proc_macro2::TokenStream as TokenStream2;
use syn::Error;

// Largely a no-op, used to flag for build.rs codegen. Only check for "first."
#[allow(non_snake_case)]
#[proc_macro_attribute]
pub fn GameScene(args: TokenStream, struct_: TokenStream) -> TokenStream {
    let args = TokenStream2::from(args);
    // args would probably parse into a syn::PathSegment, but it's not Parse
    match args.to_string().as_str() {
        "" | "first" => struct_,
        _ => args
            .into_iter()
            .map(|tree| {
                TokenStream::from(
                    Error::new_spanned(
                        &tree,
                        format!("unknown game scene attribute argument (expected: first): {tree}"),
                    )
                    .into_compile_error(),
                )
            })
            .collect(),
    }
}

#[cfg(test)]
mod tests {
    #[test]
    fn trybuild() {
        let t = trybuild::TestCases::new();
        t.pass("tests/derive/ok.rs");
        t.compile_fail("tests/derive/fail.rs")
    }
}
