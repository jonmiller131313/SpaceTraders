use anyhow::{Context, Result};

use crate::{game::Game, tui::Terminal};

mod game;
mod scenes;
mod tokens;
mod tui;
mod widgets;

#[tokio::main]
async fn main() -> Result<()> {
    let mut tui = Terminal::init().context("unable to initialize TUI")?;

    // comment

    Game::run(&mut tui)
        .await
        .context("error encountered while running the game")
}
