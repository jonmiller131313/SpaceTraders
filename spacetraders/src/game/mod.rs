use std::time::Duration;

use anyhow::{Context, Result};
use crossterm::event;
use ratatui::{prelude::*, widgets::*};
use tokio::task;

use crate::{scenes::*, tui::Terminal};

pub struct Game {
    scenes: Vec<GameSceneType>,
}
impl Game {
    const FPS: u64 = 15;

    pub async fn run(terminal: &mut Terminal) -> Result<()> {
        let scenes = vec![GameSceneType::new().await?];

        let mut game = Self { scenes };

        while !game.scenes.is_empty() {
            task::block_in_place(|| -> Result<()> {
                terminal
                    .draw(|frame| frame.render_widget(&game, frame.size()))
                    .context("error while drawing to terminal")
                    .map(|_| ())
            })?;
            game.handle_events().await.context("error while handling events")?;
        }

        Ok(())
    }

    async fn handle_events(&mut self) -> Result<()> {
        if event::poll(Duration::from_millis(1000 / Self::FPS)).context("unable to poll for events")? {
            let e = event::read().context("unable to read event")?;

            use GameSceneTransition::*;
            match self.scenes.last_mut().unwrap().handle_event(&e).await {
                Ok(Continue) => (),
                Ok(Push(new_scene)) => self.scenes.push(new_scene),
                Ok(Pop) => {
                    self.scenes.pop();
                }
                Err(e) => self.scenes.push(ErrorWidget::new(e).into()),
            }
        }
        Ok(())
    }
}
impl Widget for &Game {
    fn render(self, area: Rect, buf: &mut Buffer)
    where
        Self: Sized,
    {
        let border = Block::bordered()
            .border_type(BorderType::Double)
            .title(Line::raw("[ SpaceTraders ]").bold())
            .title_alignment(Alignment::Center);

        let inner = border.inner(area);
        border.render(area, buf);

        for scene in self.scenes.iter() {
            scene.render(inner, buf);
        }
    }
}
