use std::{env, path::PathBuf};

use anyhow::{Context, Result};
use once_cell::sync::Lazy;
use tokio::{fs, io::AsyncWriteExt};

static TOKENS_FILE: Lazy<PathBuf> =
    Lazy::new(|| PathBuf::from(env::var_os("TOKENS_FILE").unwrap_or("tokens.txt".into())));

pub async fn load() -> Result<Box<[Box<str>]>> {
    if !TOKENS_FILE.is_file() {
        return Ok(Box::new([]));
    }
    Ok(fs::read_to_string(&*TOKENS_FILE)
        .await
        .with_context(|| format!("unable to read tokens file: {}", TOKENS_FILE.display()))?
        .lines()
        .map(|line| line.to_string().into_boxed_str())
        .collect())
}

pub async fn save(token: &str) -> Result<()> {
    let mut writer = fs::File::options()
        .create(true)
        .append(true)
        .open(&*TOKENS_FILE)
        .await
        .with_context(|| format!("unable to open tokens file: {}", TOKENS_FILE.display()))?;

    writer
        .write_all(token.as_bytes())
        .await
        .with_context(|| format!("unable to write token to file: {}", TOKENS_FILE.display()))?;

    writer
        .write_u8(b'\n')
        .await
        .with_context(|| format!("unable to write newline to tokens file: {}", TOKENS_FILE.display()))
}

#[cfg(test)]
mod tests {

    #[tokio::test]
    async fn test_registration() {
        // use super::*;
        // use spacetraders_api::{RegisterNewAgent, SpaceTradersClient};
        // let client = SpaceTradersClient::new_no_auth();
        //
        // let client = RegisterNewAgent::new_default(&client, "ATH123-9").await.unwrap().4;
        // save(&client).await.unwrap();
    }
}
