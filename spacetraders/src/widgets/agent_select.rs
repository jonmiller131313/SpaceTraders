use std::cmp::Ordering;
use std::sync::Arc;

use anyhow::{Context, Result};
use futures::future::try_join_all;
use itertools::Itertools;
use num_format::{Locale, ToFormattedString};
use ratatui::{prelude::*, widgets::*};
use tokio::{
    task::{self, JoinHandle},
    try_join,
};

use spacetraders_api::{Agent, GetAgent, ListContracts, SpaceTradersClient};

use crate::tokens;

pub struct AgentSelectionWidget(Vec<AgentSelectionData>);
impl AgentSelectionWidget {
    pub async fn new() -> Result<Self> {
        let tokens: Vec<JoinHandle<Result<AgentSelectionData>>> = tokens::load()
            .await
            .context("unable to load agent tokens")?
            .into_vec()
            .into_iter()
            .enumerate()
            .map(|(i, token)| {
                task::spawn(async move {
                    AgentSelectionData::new(token)
                        .await
                        .with_context(|| format!("unable to load agent's token on line {}", i + 1))
                })
            })
            .collect();

        let tokens = try_join_all(tokens)
            .await
            .context("an agent loading thread panicked")?
            .into_iter()
            .try_collect()?;

        let mut s = Self(tokens);
        s.sort();
        Ok(s)
    }

    pub fn len(&self) -> usize {
        self.0.len()
    }

    pub async fn add_token(&mut self, token: impl Into<String>) -> Result<()> {
        let token = token.into();
        tokens::save(&token).await.context("unable to save token")?;
        self.0.push(AgentSelectionData::new(token).await?);
        self.sort();
        Ok(())
    }

    fn sort(&mut self) {
        self.0.sort_unstable();
    }

    pub fn get_agent(&self, i: usize) -> Option<(&SpaceTradersClient, &Arc<Agent>)> {
        self.0.get(i).map(|data| (&data.client, &data.agent))
    }
}

impl StatefulWidget for &AgentSelectionWidget {
    type State = usize;

    fn render(self, area: Rect, buf: &mut Buffer, selected: &mut Self::State)
    where
        Self: Sized,
    {
        let right_border = Block::default().borders(Borders::RIGHT).border_type(BorderType::Double);

        let right_inner = right_border.inner(area);
        right_border.render(area, buf);

        let agent_button_split = Layout::vertical([
            Constraint::Length(self.0.len() as u16 * 3),
            Constraint::Length(5),
            Constraint::Fill(1),
        ])
        .split(right_inner);

        render_agents(agent_button_split[0], buf, &self.0[..], *selected);
        render_new_agent_button(agent_button_split[1], buf, *selected == self.0.len());
    }
}

struct AgentSelectionData {
    client: SpaceTradersClient,
    agent: Arc<Agent>,
    num_contracts: u64,
}
impl AgentSelectionData {
    async fn new(token: impl Into<String>) -> Result<Self> {
        let token = token.into();
        let client =
            Arc::new(SpaceTradersClient::new(token.clone()).with_context(|| format!("invalid token: {token}"))?);

        let agent = task::spawn(get_agent(Arc::clone(&client)));
        let num_contracts = task::spawn(get_num_contracts(Arc::clone(&client)));

        let (agent, num_contracts) =
            try_join!(agent, num_contracts).context("tasks to load agent details and number of contracts panicked")?;

        Ok(Self {
            client: Arc::into_inner(client).expect("Joined on tasks"),
            agent: agent?.into(),
            num_contracts: num_contracts?,
        })
    }
}

impl Eq for AgentSelectionData {}

impl PartialEq<Self> for AgentSelectionData {
    fn eq(&self, rhs: &Self) -> bool {
        self.agent.symbol == rhs.agent.symbol
    }
}

impl PartialOrd<Self> for AgentSelectionData {
    fn partial_cmp(&self, rhs: &Self) -> Option<Ordering> {
        Some(self.cmp(rhs))
    }
}

impl Ord for AgentSelectionData {
    fn cmp(&self, rhs: &Self) -> Ordering {
        self.agent.symbol.cmp(&rhs.agent.symbol)
    }
}

impl Widget for &AgentSelectionData {
    fn render(self, area: Rect, buf: &mut Buffer)
    where
        Self: Sized,
    {
        let layout = Layout::vertical(Constraint::from_lengths([1, 1]))
            .horizontal_margin(1)
            .split(area);

        let upper_layout = Layout::horizontal(Constraint::from_percentages([50, 50])).split(layout[0]);
        let lower_layout = Layout::horizontal(Constraint::from_percentages([50, 50])).split(layout[1]);

        Text::from(&*self.agent.symbol).bold().render(upper_layout[0], buf);
        Text::from(format!(
            "Credits: {} §",
            self.agent.credits.to_formatted_string(&Locale::en)
        ))
        .render(lower_layout[0], buf);

        let num_ships = self.agent.ship_count.to_formatted_string(&Locale::en);
        let num_contracts = self.num_contracts.to_formatted_string(&Locale::en);
        let total_width = num_ships.len().max(num_contracts.len());

        Text::from(format!("Ships: {num_ships:>total_width$}"))
            .right_aligned()
            .render(upper_layout[1], buf);
        Text::from(format!("Contracts: {num_contracts:>total_width$}"))
            .right_aligned()
            .render(lower_layout[1], buf);
    }
}

async fn get_agent(client: Arc<SpaceTradersClient>) -> Result<Box<Agent>> {
    GetAgent::new(None)
        .get(&client)
        .await
        .context("unable to load agent data")
}

async fn get_num_contracts(client: Arc<SpaceTradersClient>) -> Result<u64> {
    ListContracts::len(&client)
        .await
        .context("unable to load the number of contracts")
}

fn render_agents(area: Rect, buf: &mut Buffer, agents: &[AgentSelectionData], selected: usize) {
    let top_title = Block::default()
        .borders(Borders::TOP)
        .title(Line::raw("[ Agents ]").bold().centered());

    let agents_area = top_title.inner(area);
    top_title.render(area, buf);

    let (agents_layout, agents_spacers) = Layout::vertical(Constraint::from_lengths(vec![2; agents.len()]))
        .spacing(1)
        .split_with_spacers(agents_area);

    agents_layout
        .iter()
        .zip(agents)
        .enumerate()
        .for_each(|(i, (&area, agent))| {
            agent.render(area, buf);
            if i == selected {
                Block::default().reversed().render(area, buf);
            }
        });

    agents_spacers
        .iter()
        .for_each(|&gap| Text::raw("──────────").centered().render(gap, buf));
}

fn render_new_agent_button(area: Rect, buf: &mut Buffer, selected: bool) {
    let border = Block::default().borders(Borders::TOP | Borders::BOTTOM);

    let inner = border.inner(area);
    border.render(area, buf);

    let button = Block::default().padding(Padding::new(0, 0, 1, 1));
    let text_area = button.inner(inner);

    Paragraph::new("Create Agent").centered().render(text_area, buf);

    if selected {
        button.reversed().render(inner, buf);
    }
}
