use anyhow::{Context, Result};
use chrono::Local;
use itertools::Itertools;
use num_format::{Locale, ToFormattedString};
use ratatui::widgets::Padding;
use ratatui::{prelude::*, widgets::*};

use spacetraders_api::{
    GetServerStatus, ServerStatus, ServerStatusAnnouncement, ServerStatusLeaderboardMostCharts,
    ServerStatusLeaderboardMostCredits, ServerStatusStats, SpaceTradersClient,
};

pub struct ServerStatusWidget(Box<ServerStatus>);
impl ServerStatusWidget {
    pub async fn new(client: impl Into<Option<&SpaceTradersClient>>) -> Result<Self> {
        Ok(Self(
            if let Some(client) = client.into() {
                GetServerStatus::get(client).await
            } else {
                GetServerStatus::get(&SpaceTradersClient::new_no_auth()).await
            }
            .context("unable to get server status")?,
        ))
    }
}
impl Widget for &ServerStatusWidget {
    fn render(self, area: Rect, buf: &mut Buffer)
    where
        Self: Sized,
    {
        let left_border = Block::default().borders(Borders::LEFT).border_type(BorderType::Double);

        let left_inner = left_border.inner(area);
        left_border.render(area, buf);

        let top_bottom_split = Layout::vertical([Constraint::Max(30), Constraint::Min(3)]).split(left_inner);

        let top_border = Block::new()
            .borders(Borders::TOP)
            .title(Line::raw("[ Server Status ]").bold().centered())
            .padding(Padding::uniform(1));

        let top_inner = top_border.inner(top_bottom_split[0]);
        top_border.render(left_inner, buf);

        let top_layouts = Layout::vertical([
            Constraint::Min(1),
            Constraint::Length(3),
            Constraint::Max(5),
            Constraint::Max(11),
            Constraint::Max(6),
        ])
        .spacing(1)
        .split(top_inner);

        Paragraph::new(&*self.0.status)
            .alignment(Alignment::Center)
            .bold()
            .render(top_layouts[0], buf);

        render_wipes(top_layouts[1], buf, &self.0);
        render_stats(top_layouts[2], buf, &self.0.stats);
        render_credits_leaderboard(top_layouts[3], buf, &self.0.leaderboards.most_credits);
        render_charts_leaderboard(top_layouts[4], buf, &self.0.leaderboards.most_submitted_charts);
        render_announcement(top_bottom_split[1], buf, &self.0.announcements);
    }
}

fn render_wipes(area: Rect, buf: &mut Buffer, status: &ServerStatus) {
    const DATETIME_FMT: &str = "%a, %d %b %Y %T %z";

    let rows = [
        Row::new([
            Text::raw("Currently:").bold(),
            Text::raw(Local::now().format(DATETIME_FMT).to_string()),
        ]),
        Row::new([
            Text::raw("Next Wipe:").bold(),
            Text::raw(
                status
                    .server_resets
                    .next
                    .with_timezone(&Local)
                    .format(DATETIME_FMT)
                    .to_string(),
            ),
        ]),
        Row::new([
            Text::raw("Last Wipe:").bold(),
            Text::raw(status.reset_date.format("%a, %d %b %Y").to_string()),
        ]),
    ];
    let wipes = Table::new(rows, [Constraint::Length(10), Constraint::Fill(1)]);
    Widget::render(wipes, area, buf);
}

fn render_stats(area: Rect, buf: &mut Buffer, stats: &ServerStatusStats) {
    let data = [
        ("agents:", stats.agents.to_formatted_string(&Locale::en)),
        ("ships:", stats.ships.to_formatted_string(&Locale::en)),
        ("systems:", stats.systems.to_formatted_string(&Locale::en)),
        ("waypoints:", stats.waypoints.to_formatted_string(&Locale::en)),
    ];
    let widest_value = data.iter().map(|(_, value)| value.len()).max().unwrap();

    let rows = [Row::new([Text::raw("Statistics:").bold()])]
        .into_iter()
        .chain(data.into_iter().map(|(first, second)| {
            Row::new([
                Text::from(first).right_aligned(),
                Text::raw(second).right_aligned(),
                "".into(),
            ])
        }));

    let stats = Table::new(
        rows,
        [
            Constraint::Length(11),
            Constraint::Length(widest_value as _),
            Constraint::Fill(1),
        ],
    );
    Widget::render(stats, area, buf);
}

fn render_credits_leaderboard(area: Rect, buf: &mut Buffer, credits: &[ServerStatusLeaderboardMostCredits]) {
    let layouts = Layout::vertical([Constraint::Length(1), Constraint::Fill(1)]).split(area);
    Paragraph::new(Text::raw("Credits Leaderboard:").bold()).render(layouts[0], buf);

    if credits.is_empty() {
        Paragraph::new("   *** EMPTY ***").render(layouts[1], buf);
        return;
    }

    let data = credits
        .iter()
        .enumerate()
        .take_while(|(i, _)| *i < 10)
        .map(|(i, data)| {
            (
                format!("{}:", i + 1),
                &*data.agent_symbol,
                data.credits.to_formatted_string(&Locale::en),
            )
        })
        .collect_vec();
    let widest_name_len = data.iter().map(|(_, name, _)| name.len()).max().unwrap();
    let widest_credits_len = data.iter().map(|(_, _, credits)| credits.len()).max().unwrap();

    let rows = data.into_iter().map(|(i, name, credits)| {
        Row::new([
            Text::from(i).right_aligned(),
            Text::from(name),
            Text::from(format!("({credits:>widest_credits_len$} §)")).left_aligned(),
        ])
    });

    let leaderboard = Table::new(
        rows,
        [
            Constraint::Length(4),
            Constraint::Length(widest_name_len as _),
            Constraint::Fill(1),
        ],
    );
    Widget::render(leaderboard, layouts[1], buf);
}

fn render_charts_leaderboard(area: Rect, buf: &mut Buffer, charts: &[ServerStatusLeaderboardMostCharts]) {
    let layouts = Layout::vertical([Constraint::Length(1), Constraint::Fill(1)]).split(area);
    Paragraph::new(Text::raw("Charts Leaderboard:").bold()).render(layouts[0], buf);

    if charts.is_empty() {
        Paragraph::new("   *** EMPTY ***").render(layouts[1], buf);
        return;
    }

    let data = charts
        .iter()
        .enumerate()
        .take_while(|(i, _)| *i < 5)
        .map(|(i, data)| {
            (
                format!("{}:", i + 1),
                &*data.agent_symbol,
                data.chart_count.to_formatted_string(&Locale::en),
            )
        })
        .collect_vec();
    let widest_name_len = data.iter().map(|(_, name, _)| name.len()).max().unwrap();
    let widest_charts_len = data.iter().map(|(_, _, charts)| charts.len()).max().unwrap();

    let rows = data.into_iter().map(|(i, name, credits)| {
        Row::new([
            Text::from(i).right_aligned(),
            Text::from(name),
            Text::from(format!("{credits:>widest_charts_len$}")).left_aligned(),
        ])
    });

    let leaderboard = Table::new(
        rows,
        [
            Constraint::Length(4),
            Constraint::Length(widest_name_len as _),
            Constraint::Fill(1),
        ],
    );
    Widget::render(leaderboard, layouts[1], buf);
}

fn render_announcement(area: Rect, buf: &mut Buffer, announcements: &[ServerStatusAnnouncement]) {
    if announcements.is_empty() {
        return;
    }

    let top_border = Block::default()
        .title(Line::raw("[ Announcements ]").bold())
        .title_alignment(Alignment::Center)
        .borders(Borders::TOP)
        .padding(Padding::new(1, 1, 1, 0));

    let inner = top_border.inner(area);
    top_border.render(area, buf);

    let mut layouts = announcements
        .iter()
        .flat_map(|_| [Constraint::Length(1), Constraint::Min(1)])
        .collect_vec();
    *layouts.last_mut().unwrap() = Constraint::Fill(2);

    let layouts = Layout::vertical(layouts).spacing(1).split(inner);

    for (i, ServerStatusAnnouncement { title, body }) in announcements.iter().enumerate() {
        Text::raw(title.as_ref()).centered().bold().render(layouts[i * 2], buf);

        Paragraph::new(body.as_ref())
            .centered()
            .wrap(Wrap { trim: false })
            .render(layouts[i * 2 + 1], buf);
    }
}
