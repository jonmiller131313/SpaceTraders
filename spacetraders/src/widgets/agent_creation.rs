use anyhow::Result;
use once_cell::sync::Lazy;
use ratatui::widgets::Padding;
use ratatui::{
    prelude::*,
    widgets::{Block, BorderType, Paragraph},
};
use strum::VariantArray;

use spacetraders_api::{Agent, Contract, Faction, FactionSymbol, RegisterNewAgent, Ship};

static FACTION_MAX_LEN: Lazy<usize> =
    Lazy::new(|| FactionSymbol::VARIANTS.iter().map(|f| f.as_ref().len()).max().unwrap());

#[derive(Default)]
pub struct AgentCreationWidget {
    faction_index: usize,
    symbol: String,
    // TODO: email?
}
impl AgentCreationWidget {
    const SYMBOL_MAX_LEN: usize = 14;

    pub fn new() -> Self {
        Self::default()
    }

    pub fn size() -> (usize, usize) {
        debug_assert!(*FACTION_MAX_LEN < Self::SYMBOL_MAX_LEN);
        (31 + 4, 4)
    }

    fn has_valid_symbol(&self) -> bool {
        (3..=14).contains(&self.symbol.len())
    }

    pub fn push(&mut self, ch: char) {
        if self.symbol.len() < Self::SYMBOL_MAX_LEN {
            self.symbol.push(ch);
        }
    }

    pub fn pop(&mut self) {
        self.symbol.pop();
    }

    pub fn clear(&mut self) {
        self.symbol.clear();
    }

    pub fn next_faction(&mut self) {
        self.faction_index = (self.faction_index + 1).min(FactionSymbol::VARIANTS.len() - 1);
    }

    pub fn prev_faction(&mut self) {
        self.faction_index = self.faction_index.saturating_sub(1);
    }

    pub async fn register_agent(
        &self,
    ) -> Result<Option<(Box<Agent>, Box<Contract>, Box<Faction>, Box<Ship>, Box<str>)>> {
        Ok(if !self.has_valid_symbol() {
            None
        } else {
            Some(
                RegisterNewAgent::builder(self.symbol.clone())
                    .faction(FactionSymbol::VARIANTS[self.faction_index])
                    .build()
                    .unwrap()
                    .submit()
                    .await?,
            )
        })
    }
}
impl Widget for &AgentCreationWidget {
    fn render(self, area: Rect, buf: &mut Buffer)
    where
        Self: Sized,
    {
        let left_arrow = if self.faction_index == 0 { ' ' } else { '◀' };
        let right_arrow = if self.faction_index == FactionSymbol::VARIANTS.len() - 1 {
            ' '
        } else {
            '▶'
        };

        Paragraph::new(vec![
            vec![
                Span::raw("Faction: ").bold(),
                format!(
                    "{left_arrow}  {:^FACTION_MAX_LEN$} {right_arrow}",
                    FactionSymbol::VARIANTS[self.faction_index].as_ref()
                )
                .into(),
            ]
            .into(),
            vec![Span::raw("Agent: ").bold(), format!("{}█", self.symbol).into()].into(),
        ])
        .block(
            Block::bordered()
                .border_type(BorderType::Rounded)
                .padding(Padding::horizontal(1))
                .title_top(Line::raw("[ Create new agent ]").centered())
                .title_bottom(
                    Line::from(vec![
                        " Submit: <".into(),
                        Span::styled(
                            "ENTER",
                            Style::default().fg(if self.has_valid_symbol() {
                                Color::LightGreen
                            } else {
                                Color::LightRed
                            }),
                        ),
                        "> Cancel: <ESC> ".into(),
                    ])
                    .centered(),
                ),
        )
        .render(area, buf);
    }
}
