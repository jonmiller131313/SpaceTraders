use std::{
    io::{self, Stdout},
    mem,
    ops::{Deref, DerefMut, Drop},
    panic,
    sync::Once,
};

use anyhow::{Context, Result};
use crossterm::{
    cursor, execute,
    terminal::{disable_raw_mode, enable_raw_mode, EnterAlternateScreen, LeaveAlternateScreen},
};
use ratatui::prelude::{Terminal as RatTerminal, *};

type InnerTerminal = RatTerminal<CrosstermBackend<Stdout>>;

static SETUP_PANIC_HOOK: Once = Once::new();

#[repr(transparent)]
pub struct Terminal(InnerTerminal);
impl Terminal {
    pub fn init() -> Result<Self> {
        let mut stdout = io::stdout();

        execute!(&mut stdout, EnterAlternateScreen).context("unable to enter alternative terminal screen")?;
        enable_raw_mode().context("unable to enter raw mode")?;

        let mut terminal =
            InnerTerminal::new(CrosstermBackend::new(stdout)).context("unable to initialize TUI library")?;
        terminal.clear().context("unable to clear terminal")?;

        SETUP_PANIC_HOOK.call_once(setup_panic_hook);

        Ok(Self(terminal))
    }

    fn cleanup_inner() -> Result<()> {
        execute!(io::stdout(), LeaveAlternateScreen, cursor::Show)
            .context("unable to leave alternate terminal screen and show cursor")?;
        disable_raw_mode().context("unable to disable raw mode")
    }

    pub fn cleanup(self) -> Result<()> {
        Self::cleanup_inner()?;

        mem::forget(self);
        Ok(())
    }
}
impl Drop for Terminal {
    fn drop(&mut self) {
        if let Err(e) = Self::cleanup_inner() {
            eprintln!("WARNING: Unable to cleanup terminal: {e}");
        }
    }
}
impl Deref for Terminal {
    type Target = InnerTerminal;
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl DerefMut for Terminal {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}

fn setup_panic_hook() {
    let orig_hook = panic::take_hook();
    panic::set_hook(Box::new(move |panic_info| {
        if let Err(e) = Terminal::cleanup_inner() {
            eprintln!("WARNING: Unable to cleanup terminal: {e}");
        }
        orig_hook(panic_info)
    }))
}
