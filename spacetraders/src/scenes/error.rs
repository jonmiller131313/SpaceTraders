use anyhow::{Error, Result};
use crossterm::event::Event;
use ratatui::{prelude::*, widgets::*};

use spacetraders_api::error::{ApiError, ServerError};

use game_scene_gen_derive::GameScene;

use super::{GameScene, GameSceneTransition};

#[GameScene]
pub struct ErrorWidget {
    error: Error,

    show_backtrace: bool,
}
impl ErrorWidget {
    pub fn new(error: Error) -> Self {
        Self {
            error,
            show_backtrace: false,
        }
    }

    fn get_server_error(&self) -> Option<&ServerError> {
        self.error.chain().find_map(|e| {
            e.downcast_ref::<ApiError>().and_then(|error| {
                if let ApiError::Server(error) = error {
                    Some(error)
                } else {
                    None
                }
            })
        })
    }
}

impl Widget for &ErrorWidget {
    fn render(self, area: Rect, buf: &mut Buffer)
    where
        Self: Sized,
    {
        let error = self.get_server_error();

        let window = Block::bordered()
            .style(Style::default().bg(Color::Red).fg(Color::LightYellow))
            .border_type(BorderType::Double)
            .padding(Padding::uniform(1))
            .title_top(
                Line::raw(
                    error
                        .map(|err| format!(" The server returned an error (HTTP code: {}) ", err.code))
                        .unwrap_or(" An unknown error occurred ".into()),
                )
                .centered(),
            )
            .title_bottom(Line::raw(" Panic: <ENTER> Backtrace: <B> Ignore: <ESC> ").right_aligned());

        let middle = Layout::horizontal(Constraint::from_percentages([25, 50, 25])).split(area)[1];
        let layout = Layout::vertical(Constraint::from_percentages([12, 75, 13])).split(middle)[1];

        let text = if self.show_backtrace {
            Text::raw(format!("{:?}", self.error))
        } else {
            error
                .map(|err| {
                    Text::from(vec![
                        Line::raw(&*err.title).bold(),
                        Line::default(),
                        Line::raw(&*err.description),
                    ])
                })
                .unwrap_or_else(|| Text::raw(self.error.to_string()))
        };

        Clear {}.render(layout, buf);
        Paragraph::new(text)
            .block(window)
            .wrap(Wrap::default())
            .render(layout, buf);
    }
}

impl<'a> GameScene<'a> for ErrorWidget {
    async fn handle_event(&'a mut self, event: &Event) -> Result<GameSceneTransition> {
        use crossterm::event::{
            KeyCode::{Char, Enter, Esc},
            KeyEvent,
        };
        use Event::Key;

        Ok(match event {
            Key(KeyEvent { code: Enter, .. }) => panic!("{:?}", self.error),
            Key(KeyEvent { code: Char('b'), .. }) => {
                self.show_backtrace ^= true;
                GameSceneTransition::Continue
            }
            Key(KeyEvent { code: Esc, .. }) => GameSceneTransition::Pop,
            _ => GameSceneTransition::Continue,
        })
    }
}
