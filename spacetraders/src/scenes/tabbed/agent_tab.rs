use num_format::{Locale, ToFormattedString};
use ratatui::{layout::*, prelude::*, widgets::*};

use crate::scenes::TabbedWidget;

pub fn render(data: &TabbedWidget, area: Rect, buf: &mut Buffer) {
    let [left, contracts_area] = *Layout::horizontal(Constraint::from_percentages([50, 50])).split(area) else {
        unreachable!()
    };
    let [agent_area, faction_area] = *Layout::vertical([Constraint::Max(13), Constraint::Fill(1)]).split(left) else {
        unreachable!()
    };

    let agent_window = Block::bordered()
        .border_type(BorderType::Rounded)
        .padding(Padding::uniform(1))
        .title(" Agent ");
    let agent_area_ = agent_window.inner(agent_area);
    agent_window.render(agent_area, buf);
    render_agent(data, agent_area_, buf);

    let faction_window = Block::bordered()
        .border_type(BorderType::Rounded)
        .padding(Padding::uniform(1))
        .title(" Faction ");
    let faction_area_ = faction_window.inner(faction_area);
    faction_window.render(faction_area, buf);
    render_faction(data, faction_area_, buf);

    let contracts_window = Block::bordered()
        .border_type(BorderType::Rounded)
        .padding(Padding::uniform(1))
        .title(" Contracts ");
    let contracts_area_ = contracts_window.inner(contracts_area);
    contracts_window.render(contracts_area, buf);
    render_contracts(data, contracts_area_, buf);
}

fn render_agent(data: &TabbedWidget, area: Rect, buf: &mut Buffer) {
    let [agent_area, id_area, credits_area, num_ships_area, hq_area] =
        *Layout::vertical(Constraint::from_lengths([1; 5]))
            .spacing(1)
            .split(area)
    else {
        unreachable!()
    };

    Text::from_iter([Span::raw(&*data.agent.symbol).bold().underlined(), Span::default()]).render(agent_area, buf);

    Text::raw(format!(
        "ID:      {}",
        data.agent
            .account_id
            .as_ref()
            .expect("Self agent should have account ID")
    ))
    .render(id_area, buf);

    Text::raw(format!(
        "Credits: {} §",
        data.agent.credits.to_formatted_string(&Locale::en)
    ))
    .render(credits_area, buf);

    Text::raw(format!("# Ships: {}", data.ships.len())).render(num_ships_area, buf);

    Text::raw(format!("HQ:      {}", &*data.agent.headquarters)).render(hq_area, buf);
}

fn render_faction(data: &TabbedWidget, area: Rect, buf: &mut Buffer) {
    let faction = data.faction.as_ref();

    let [name_area, description_area, hq_area, recruiting_area, traits_area] =
        *Layout::vertical(Constraint::from_maxes([1, 3, 1, 1, 1]))
            .spacing(1)
            .split(area)
    else {
        unreachable!()
    };

    let symbol: &str = faction.symbol.into();
    Text::from_iter([
        Span::raw(format!("{} ({symbol})", &*faction.name)).bold().underlined(),
        Span::default(),
    ])
    .render(name_area, buf);

    Paragraph::new(&*faction.description)
        .wrap(Wrap::default())
        .render(description_area, buf);

    Text::raw(format!("HQ: {}", &*faction.headquarters)).render(hq_area, buf);

    Text::raw(format!(
        "Is Recruiting? {}",
        if faction.is_recruiting { "yes" } else { "no" }
    ))
    .render(recruiting_area, buf);

    Text::raw(format!("Num Traits: {}", faction.traits.len())).render(traits_area, buf);

    /*
    Traits:

    Bureaucratic
      a;lskdfa;lskdfalksjdalks dfalksdjlaksj flkaj sflakj sf
      asd fasdf asdf asfda sdf
      a sdfasd fasdf

    Secretive
      asdlfkj aslkdj alk sjdflka jsdlkfj alksdfjasdf
      as dfasd fads flkasdj flkaj falkdsjfads
      f daf kajsdflkjasdflk jasdflkj asdf
        */
}

fn render_contracts(data: &TabbedWidget, area: Rect, buf: &mut Buffer) {}
