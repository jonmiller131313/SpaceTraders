use std::{iter, sync::Arc};

use anyhow::{Context, Result};
use crossterm::event::Event;
use itertools::izip;
use ratatui::{prelude::*, widgets::*};
use strum::{EnumIter, IntoEnumIterator};

use game_scene_gen_derive::GameScene;

use spacetraders_api::*;

use super::codegen::*;

mod agent_tab;
mod ships_tab;

#[GameScene]
pub struct TabbedWidget {
    current_tab: Tabs,

    client: SpaceTradersClient,

    agent: Arc<Agent>,
    faction: Arc<Faction>,
    ships: Arc<[Ship]>,
}
impl TabbedWidget {
    pub async fn new(client: SpaceTradersClient, agent: Arc<Agent>) -> Result<Self> {
        let faction = GetFaction::new(agent.starting_faction)
            .get(&client)
            .await
            .context("unable to load agent faction")?
            .into();
        let ships = ListShips::get(&client, None)
            .await
            .context("unable to load ships")?
            .into();
        Ok(Self {
            current_tab: Tabs::Agent,
            client,
            agent,
            faction,
            ships,
        })
    }
}

impl Widget for &TabbedWidget {
    fn render(self, area: Rect, buf: &mut Buffer)
    where
        Self: Sized,
    {
        Clear {}.render(area, buf);

        let [tab_layout, content_layout] = *Layout::vertical([Constraint::Length(2), Constraint::Fill(1)]).split(area)
        else {
            unreachable!();
        };
        self.current_tab.render(tab_layout, buf);

        let render_fn: &dyn Fn(Self, Rect, &mut Buffer) = match self.current_tab {
            Tabs::Agent => &agent_tab::render,
            Tabs::Ships => &ships_tab::render,
            _ => todo!(),
        };
        render_fn(self, content_layout, buf);
    }
}

impl<'a> GameScene<'a> for TabbedWidget {
    async fn handle_event(&'a mut self, event: &Event) -> Result<GameSceneTransition> {
        use crossterm::event::{
            Event::Key,
            KeyCode::{Char, Esc},
            KeyEvent, KeyModifiers,
        };
        Ok(match event {
            Key(KeyEvent { code: Esc, .. }) => GameSceneTransition::Pop,
            Key(KeyEvent {
                code: Char(ch),
                modifiers: KeyModifiers::CONTROL,
                ..
            }) => {
                match *ch {
                    'a' => self.current_tab = Tabs::Agent,
                    's' => self.current_tab = Tabs::Ships,
                    // Ctrl+M maps to Enter: https://github.com/crossterm-rs/crossterm/issues/633
                    'y' => self.current_tab = Tabs::SystemMap,
                    'u' => self.current_tab = Tabs::UniverseMap,
                    _ => (),
                }
                GameSceneTransition::Continue
            }
            _ => GameSceneTransition::Continue,
        })
    }
}

#[derive(Clone, Copy, EnumIter, PartialEq)]
enum Tabs {
    Agent,
    Ships,
    SystemMap,
    UniverseMap,
}
impl Widget for Tabs {
    fn render(self, area: Rect, buf: &mut Buffer) {
        let bottom_tab_bar = Block::default().borders(Borders::BOTTOM).border_type(BorderType::Thick);

        let tab_text_area = bottom_tab_bar.inner(area);
        bottom_tab_bar.render(area, buf);

        let tab_contents = [("Agent", 0), ("Ships", 0), ("System Map", 1), ("Universe Map", 0)];

        let (tabs, spacers) = Layout::horizontal(
            Constraint::from_lengths(tab_contents.iter().map(|(text, _)| text.len() as u16 + 6))
                .iter()
                .chain(iter::once(&Constraint::Fill(1))),
        )
        .spacing(1)
        .split_with_spacers(tab_text_area);

        let tab_text: Vec<_> = tab_contents
            .iter()
            .map(|(text, shortcut_index)| create_tab_text(text, *shortcut_index))
            .collect();

        for (text, mut layout, mut spacer, tab) in izip!(
            tab_text,
            tabs.iter().copied(),
            spacers.iter().skip(1).copied(),
            Tabs::iter()
        ) {
            text.render(layout, buf);
            Text::raw("┃").centered().render(spacer, buf);

            spacer.y += 1;
            Text::raw("┻").centered().render(spacer, buf);

            if self == tab {
                layout.y += 1;
                Clear {}.render(layout, buf);
                Text::raw("┗").render(spacer, buf);

                if self != Tabs::Agent {
                    layout.x -= 1;
                    Text::raw("┛").render(layout, buf);
                }
            }
        }
    }
}

fn create_tab_text(text: &str, shortcut_index: usize) -> Line {
    let (beginning, text) = text.split_at(shortcut_index);
    let shortcut_char = &text[..1];
    let ending = &text[1..];

    Line::from_iter([
        Span::raw(beginning),
        Span::styled(shortcut_char, Style::default().underlined().bold()),
        Span::raw(ending),
    ])
    .centered()
}
