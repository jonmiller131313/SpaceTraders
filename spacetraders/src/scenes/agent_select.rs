use std::sync::Arc;

use anyhow::{Context, Result};
use crossterm::event::Event;
use ratatui::prelude::*;
use tokio::{task, try_join};

use crate::scenes::TabbedWidget;
use game_scene_gen_derive::GameScene;

use super::codegen::{GameScene, GameSceneTransition};
use crate::widgets::{AgentCreationWidget, AgentSelectionWidget, ServerStatusWidget};

#[GameScene(first)]
pub struct AgentSelectScene {
    agent_selection: usize,

    agent_selection_widget: AgentSelectionWidget,
    status_widget: ServerStatusWidget,

    agent_creation_widget: Option<AgentCreationWidget>,
}
impl AgentSelectScene {
    pub async fn new() -> Result<Self> {
        let agents = task::spawn(AgentSelectionWidget::new());
        let server_status = task::spawn(ServerStatusWidget::new(None));

        let (agent_selection_widget, status_widget) =
            try_join!(agents, server_status).context("tasks to load initial data panicked")?;

        Ok(Self {
            agent_selection: 0,

            agent_selection_widget: agent_selection_widget?,
            status_widget: status_widget?,

            agent_creation_widget: None,
        })
    }
}

impl Widget for &AgentSelectScene {
    fn render(self, area: Rect, buf: &mut Buffer)
    where
        Self: Sized,
    {
        let layouts = Layout::horizontal([
            Constraint::Percentage(25),
            Constraint::Percentage(50),
            Constraint::Percentage(25),
        ])
        .split(area);

        self.agent_selection_widget
            .render(layouts[0], buf, &mut self.agent_selection.clone());
        self.status_widget.render(layouts[2], buf);

        if let Some(widget) = &self.agent_creation_widget {
            let (w, h) = AgentCreationWidget::size();

            let v_center_layout =
                Layout::vertical([Constraint::Fill(1), Constraint::Length(h as _), Constraint::Fill(1)])
                    .split(layouts[1])[1];
            let centered_layout =
                Layout::horizontal([Constraint::Fill(1), Constraint::Length(w as _), Constraint::Fill(1)])
                    .split(v_center_layout)[1];

            widget.render(centered_layout, buf);
        }
    }
}

impl<'a> GameScene<'a> for AgentSelectScene {
    async fn handle_event(&'a mut self, event: &Event) -> Result<GameSceneTransition> {
        use crossterm::event::{
            KeyCode::{Backspace, Char, Down, Enter, Esc, Left, Right, Up},
            KeyEvent, KeyModifiers,
        };
        use Event::Key;

        match (&mut self.agent_creation_widget, event) {
            (
                None,
                Key(KeyEvent {
                    code: Char('q') | Esc, ..
                }),
            ) => return Ok(GameSceneTransition::Pop),

            (None, Key(KeyEvent { code: Up, .. })) => {
                self.agent_selection = self.agent_selection.saturating_sub(1);
            }

            (None, Key(KeyEvent { code: Down, .. })) => {
                self.agent_selection = (self.agent_selection + 1).min(self.agent_selection_widget.len());
            }

            (None, Key(KeyEvent { code: Enter, .. })) => {
                if let Some((client, agent)) = self.agent_selection_widget.get_agent(self.agent_selection) {
                    return Ok(GameSceneTransition::Push(
                        TabbedWidget::new(client.clone(), Arc::clone(agent))
                            .await
                            .context("unable to switch to tabbed scene")?
                            .into(),
                    ));
                } else {
                    self.agent_creation_widget = Some(AgentCreationWidget::new());
                }
            }

            (
                Some(widget),
                Key(KeyEvent {
                    code: Char(ch),
                    modifiers: KeyModifiers::NONE | KeyModifiers::SHIFT,
                    ..
                }),
            ) => {
                widget.push(ch.to_uppercase().next().context("only ASCII is supported")?);
            }

            (
                Some(widget),
                Key(KeyEvent {
                    code: Backspace,
                    modifiers: KeyModifiers::NONE,
                    ..
                }),
            ) => {
                widget.pop();
            }

            (
                Some(widget),
                Key(KeyEvent {
                    // Ctrl + Backspace maps to Ctrl + H on linux
                    // See: https://github.com/crossterm-rs/crossterm/issues/723
                    code: Char('h' | 'u'),
                    modifiers: KeyModifiers::CONTROL,
                    ..
                }),
            ) => {
                widget.clear();
            }

            (Some(widget), Key(KeyEvent { code: Left, .. })) => {
                widget.prev_faction();
            }

            (Some(widget), Key(KeyEvent { code: Right, .. })) => {
                widget.next_faction();
            }

            (Some(_), Key(KeyEvent { code: Esc, .. })) => {
                self.agent_creation_widget = None;
            }

            (Some(widget), Key(KeyEvent { code: Enter, .. })) => {
                if let Some((.., new_token)) = widget.register_agent().await.context("unable to register new agent")? {
                    self.agent_selection_widget
                        .add_token(new_token)
                        .await
                        .context("unable to load new agent data")?;
                    self.agent_creation_widget = None;
                }
            }

            _ => (),
        }
        Ok(GameSceneTransition::Continue)
    }
}
