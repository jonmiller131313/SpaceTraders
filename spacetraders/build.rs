use camino::Utf8Path;

fn main() {
    let scene_mod_dir = Utf8Path::new(env!("CARGO_MANIFEST_DIR")).join("src/scenes");
    let codegen_file = scene_mod_dir.join("codegen.rs");

    game_scene_gen_impl::entrypoint(&scene_mod_dir, &codegen_file);
}
