use crate::{Agent, Contract};
use std::borrow::Cow;

use crate::prelude::*;

pub struct ListContracts;
impl ListContracts {
    pub async fn get(client: &SpaceTradersClient, range: impl Into<Option<Range>>) -> Result<Box<[Contract]>> {
        query_range(Self, client, range).await
    }

    pub async fn len(client: &SpaceTradersClient) -> Result<u64> {
        query_len(&Self, client).await
    }
}
impl Endpoint for ListContracts {
    fn endpoint(&self) -> impl Into<Cow<'static, str>> {
        "/my/contracts"
    }
}
impl Pageable for ListContracts {}

#[cfg(test)]
mod tests {
    use super::*;

    #[tokio::test]
    async fn test_list_contracts() {
        let Some(client) = get_client() else {
            return;
        };

        let contracts = ListContracts::get(&client, 0..1).await.unwrap();
        assert!(contracts.first().is_some());
    }
}
