use std::borrow::Cow;

use crate::apis::systems::ListSystems;
use crate::{models::Agent, prelude::*};

/// https://spacetraders.stoplight.io/docs/spacetraders/d4567f6f3c159-list-agents
pub struct ListAgents;
impl ListAgents {
    pub async fn get(client: &SpaceTradersClient, range: impl Into<Option<Range>>) -> Result<Box<[Agent]>> {
        query_range(Self, client, range).await
    }

    pub async fn len(client: &SpaceTradersClient) -> Result<u64> {
        query_len(&Self, client).await
    }
}
impl Endpoint for ListAgents {
    fn endpoint(&self) -> impl Into<Cow<'static, str>> {
        "/agents"
    }
}
impl Pageable for ListAgents {}

pub struct GetAgent(Option<Box<str>>);
impl GetAgent {
    /// Some: https://spacetraders.stoplight.io/docs/spacetraders/82c819018af91-get-public-agent
    /// None: https://spacetraders.stoplight.io/docs/spacetraders/eb030b06e0192-get-agent
    pub fn new(symbol: impl Into<Option<String>>) -> Self {
        Self(symbol.into().map(String::into_boxed_str))
    }

    pub async fn get(&self, client: &SpaceTradersClient) -> Result<Box<Agent>> {
        query_single(self, client).await
    }
}
impl Endpoint for GetAgent {
    fn endpoint(&self) -> impl Into<Cow<'static, str>> {
        match &self.0 {
            Some(symbol) => Cow::from(format!("/agents/{symbol}")),
            None => Cow::from("/my/agent"),
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[tokio::test]
    async fn test_get_agents() {
        let client = SpaceTradersClient::new(None).unwrap();
        let agents = ListAgents::get(&client, 15..25).await.unwrap();

        assert_eq!(agents.len(), 10);
    }

    #[tokio::test]
    async fn test_get_public_agent() {
        let client = SpaceTradersClient::new(None).unwrap();
        let agents = ListAgents::get(&client, 0..1).await.unwrap();
        let agent = GetAgent::new(agents[0].symbol.to_string()).get(&client).await.unwrap();

        assert_eq!(agent.credits, agents[0].credits);
    }

    #[tokio::test]
    async fn test_get_agent() {
        let Some(client) = get_client() else {
            return;
        };
        let agents = ListAgents::get(&client, 0..1).await.unwrap();
        assert!(agents.first().is_some());

        // dbg!(agents[0].)
    }
}
