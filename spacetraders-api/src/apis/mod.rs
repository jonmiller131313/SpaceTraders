use std::borrow::Cow;

use chrono::NaiveDate;
use derive_builder::Builder;
use reqwest::Method;
use serde::{Deserialize, Serialize};
use serde_json::Value as JsonValue;
use url::Url;

use crate::{prelude::*, Agent, ApiError, Contract, Faction, FactionSymbol, ServerStatus, Ship};

mod agents;
mod contracts;
mod factions;
mod fleet;
mod systems;

pub use self::{agents::*, contracts::*, factions::*, fleet::*, systems::*};

/// https://spacetraders.stoplight.io/docs/spacetraders/7534550e1ba52-get-status
pub struct GetServerStatus;
impl GetServerStatus {
    pub async fn get(client: &SpaceTradersClient) -> Result<Box<ServerStatus>> {
        let e = GetServerStatus;
        e.query(client).await
    }
}
impl Endpoint for GetServerStatus {
    fn endpoint(&self) -> impl Into<Cow<'static, str>> {
        "/"
    }
}

/// https://spacetraders.stoplight.io/docs/spacetraders/86ed6bbe4f5d7-register-new-agent
#[derive(Builder, Serialize)]
#[builder(pattern = "owned", setter(into))]
pub struct RegisterNewAgent {
    #[builder(default)]
    faction: FactionSymbol,
    symbol: String,
    #[builder(setter(strip_option), default)]
    email: Option<String>,
}
impl RegisterNewAgent {
    pub async fn new_default(
        symbol: impl Into<String>,
    ) -> Result<(Box<Agent>, Box<Contract>, Box<Faction>, Box<Ship>, Box<str>)> {
        let symbol = symbol.into();
        Self::builder(&symbol)
            .build()
            .map_err(|_| ApiError::InvalidSymbol(symbol.into_boxed_str()))?
            .submit()
            .await
    }

    pub fn builder(symbol: impl Into<String>) -> RegisterNewAgentBuilder {
        RegisterNewAgentBuilder::default().symbol(symbol.into())
    }

    pub async fn submit(&self) -> Result<(Box<Agent>, Box<Contract>, Box<Faction>, Box<Ship>, Box<str>)> {
        #[derive(Deserialize)]
        struct ResponseData {
            data: ResponseDataData,
        }
        #[derive(Deserialize)]
        struct ResponseDataData {
            agent: Box<Agent>,
            contract: Box<Contract>,
            faction: Box<Faction>,
            ship: Box<Ship>,
            token: Box<str>,
        }
        let response: ResponseData = self.query(&SpaceTradersClient::new_no_auth()).await?;
        Ok((
            response.data.agent,
            response.data.contract,
            response.data.faction,
            response.data.ship,
            response.data.token,
        ))
    }
}
impl Endpoint for RegisterNewAgent {
    fn method(&self) -> Method {
        Method::POST
    }

    fn endpoint(&self) -> impl Into<Cow<'static, str>> {
        "/register"
    }

    fn body(&self) -> Option<JsonValue> {
        Some(serde_json::to_value(self).unwrap())
    }
}

impl RegisterNewAgentBuilder {
    fn validate(&self) -> Result<()> {
        if let Some(symbol) = &self.symbol {
            if (3..=14).contains(&symbol.len()) {
                Ok(())
            } else {
                Err(ApiError::InvalidSymbol(symbol.clone().into_boxed_str()))
            }
        } else {
            unreachable!()
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[tokio::test]
    async fn test_status() {
        let client = SpaceTradersClient::new(None).unwrap();
        let status = GetServerStatus::get(&client).await.unwrap();
        assert_eq!(&*status.version, "v2.2.0");
    }
}
