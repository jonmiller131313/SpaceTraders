use std::borrow::Cow;

use crate::{prelude::*, Endpoint, Pageable, Range, Result, Ship, SpaceTradersClient};

pub struct GetShip(Box<str>);
impl GetShip {
    pub fn new(symbol: impl Into<Box<str>>) -> Self {
        Self(symbol.into())
    }

    pub async fn get(&self, client: &SpaceTradersClient) -> Result<Box<Ship>> {
        query_single(self, client).await
    }
}
impl Endpoint for GetShip {
    fn endpoint(&self) -> impl Into<Cow<'static, str>> {
        format!("/my/ships/{}", self.0)
    }
}

pub struct ListShips;
impl ListShips {
    pub async fn get(client: &SpaceTradersClient, range: impl Into<Option<Range>>) -> Result<Box<[Ship]>> {
        query_range(Self, client, range).await
    }

    pub async fn len(client: &SpaceTradersClient) -> Result<u64> {
        query_len(&Self, client).await
    }
}
impl Endpoint for ListShips {
    fn endpoint(&self) -> impl Into<Cow<'static, str>> {
        "/my/ships"
    }
}
impl Pageable for ListShips {}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::{prelude::*, GetAgent, SpaceTradersClient};

    #[tokio::test]
    async fn test_get_ship() {
        let Some(client) = get_client() else {
            return;
        };

        let ships = ListShips::get(&client, 0..1).await.unwrap();
        assert!(ships.first().is_some());
    }
}
