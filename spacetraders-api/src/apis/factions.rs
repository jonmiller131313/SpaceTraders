use std::borrow::Cow;

use crate::{models::Faction, prelude::*, FactionSymbol};

pub struct ListFactions;
impl ListFactions {
    pub async fn get(client: &SpaceTradersClient, range: impl Into<Option<Range>>) -> Result<Box<[Faction]>> {
        query_range(Self, client, range).await
    }

    pub async fn len(client: &SpaceTradersClient) -> Result<u64> {
        query_len(&Self, client).await
    }
}
impl Endpoint for ListFactions {
    fn endpoint(&self) -> impl Into<Cow<'static, str>> {
        "/factions"
    }
}
impl Pageable for ListFactions {}

pub struct GetFaction(FactionSymbol);
impl GetFaction {
    pub fn new(symbol: FactionSymbol) -> Self {
        Self(symbol)
    }

    pub async fn get(&self, client: &SpaceTradersClient) -> Result<Box<Faction>> {
        query_single(self, client).await
    }
}
impl Endpoint for GetFaction {
    fn endpoint(&self) -> impl Into<Cow<'static, str>> {
        let symbol: &str = self.0.into();
        format!("/factions/{symbol}")
    }
}
