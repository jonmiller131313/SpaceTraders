use std::borrow::Cow;

use crate::apis::agents::ListAgents;
use crate::{models::System, prelude::*};

/// https://spacetraders.stoplight.io/docs/spacetraders/94269411483d0-list-systems
pub struct ListSystems;
impl ListSystems {
    pub async fn get(client: &SpaceTradersClient, range: impl Into<Option<Range>>) -> Result<Box<[System]>> {
        query_range(Self, client, range).await
    }

    pub async fn len(client: &SpaceTradersClient) -> Result<u64> {
        query_len(&Self, client).await
    }
}
impl Endpoint for ListSystems {
    fn endpoint(&self) -> impl Into<Cow<'static, str>> {
        "/systems"
    }
}
impl Pageable for ListSystems {}

#[cfg(test)]
mod tests {
    use super::*;

    #[tokio::test]
    async fn test_get_agents() {
        let client = SpaceTradersClient::new(None).unwrap();
        let systems = ListSystems::get(&client, 15..25).await.unwrap();
        assert_eq!(systems.len(), 10);
    }
}
