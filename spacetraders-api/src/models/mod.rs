use chrono::NaiveDate;
use serde::Deserialize;
use url::Url;

use crate::Timestamp;

mod agents;
mod contracts;
mod factions;
mod fleet;
mod systems;

pub use self::{agents::*, contracts::*, factions::*, fleet::*, systems::*};

#[derive(Debug, Deserialize)]
#[serde(rename_all = "camelCase", deny_unknown_fields)]
pub struct ServerStatus {
    pub status: Box<str>,
    pub version: Box<str>,
    pub reset_date: NaiveDate,
    pub description: Box<str>,
    pub stats: ServerStatusStats,
    pub leaderboards: ServerStatusLeaderboards,
    pub server_resets: ServerStatusServerReset,
    pub announcements: Box<[ServerStatusAnnouncement]>,
    pub links: Box<[ServerStatusLink]>,
}

#[derive(Debug, Deserialize)]
#[serde(deny_unknown_fields)]
pub struct ServerStatusStats {
    pub agents: u64,
    pub ships: u64,
    pub systems: u64,
    pub waypoints: u64,
}

#[derive(Debug, Deserialize)]
#[serde(rename_all = "camelCase", deny_unknown_fields)]
pub struct ServerStatusLeaderboards {
    pub most_credits: Box<[ServerStatusLeaderboardMostCredits]>,
    pub most_submitted_charts: Box<[ServerStatusLeaderboardMostCharts]>,
}

#[derive(Debug, Deserialize)]
#[serde(rename_all = "camelCase", deny_unknown_fields)]
pub struct ServerStatusLeaderboardMostCredits {
    pub agent_symbol: Box<str>,
    pub credits: i64,
}

#[derive(Debug, Deserialize)]
#[serde(rename_all = "camelCase", deny_unknown_fields)]
pub struct ServerStatusLeaderboardMostCharts {
    pub agent_symbol: Box<str>,
    pub chart_count: u64,
}

#[derive(Debug, Deserialize)]
#[serde(deny_unknown_fields)]
pub struct ServerStatusServerReset {
    pub next: Timestamp,
    pub frequency: Box<str>,
}

#[derive(Debug, Deserialize)]
#[serde(deny_unknown_fields)]
pub struct ServerStatusAnnouncement {
    pub title: Box<str>,
    pub body: Box<str>,
}

#[derive(Debug, Deserialize)]
#[serde(deny_unknown_fields)]
pub struct ServerStatusLink {
    pub name: Box<str>,
    pub url: Url,
}
