use serde::Deserialize;

use crate::FactionSymbol;

#[derive(Debug, Deserialize)]
#[serde(rename_all = "camelCase", deny_unknown_fields)]
pub struct System {
    pub symbol: Box<str>,
    pub sector_symbol: Box<str>,
    #[serde(rename = "type")]
    pub type_: SystemType,
    pub x: i64,
    pub y: i64,
    pub waypoints: Box<[SystemWaypoint]>,
    pub factions: Box<[SystemFaction]>,
}

#[derive(Debug, Deserialize)]
#[serde(rename_all = "SCREAMING_SNAKE_CASE")]
pub enum SystemType {
    NeutronStar,
    RedStar,
    OrangeStar,
    BlueStar,
    YoungStar,
    WhiteDwarf,
    BlackHole,
    Hypergiant,
    Nebula,
    Unstable,
}

#[derive(Debug, Deserialize)]
#[serde(deny_unknown_fields)]
pub struct SystemWaypoint {
    pub symbol: Box<str>,
    #[serde(rename = "type")]
    pub type_: WaypointType,
    pub x: i64,
    pub y: i64,
    pub orbitals: Box<[SystemWaypointsOrbital]>,
    pub orbits: Option<Box<str>>,
}

#[derive(Debug, Deserialize)]
#[serde(rename_all = "SCREAMING_SNAKE_CASE")]
pub enum WaypointType {
    Planet,
    GasGiant,
    Moon,
    OrbitalStation,
    JumpGate,
    AsteroidField,
    Asteroid,
    EngineeredAsteroid,
    AsteroidBase,
    Nebula,
    DebrisField,
    GravityWell,
    ArtificialGravityWell,
    FuelStation,
}

#[derive(Debug, Deserialize)]
#[serde(deny_unknown_fields)]
pub struct SystemWaypointsOrbital {
    pub symbol: Box<str>,
}

#[derive(Debug, Deserialize)]
#[serde(deny_unknown_fields)]
pub struct SystemFaction {
    pub symbol: FactionSymbol,
}
