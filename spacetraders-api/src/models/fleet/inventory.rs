use std::num::NonZeroU64;

use serde::Deserialize;

#[derive(Debug, Deserialize)]
#[serde(deny_unknown_fields)]
pub struct ShipCargo {
    pub capacity: u64,
    pub units: u64,
    pub inventory: Box<[ShipCargoGood]>,
}

#[derive(Debug, Deserialize)]
#[serde(deny_unknown_fields)]
pub struct ShipCargoGood {
    // TODO: Figure out how to handle all of these
    pub symbol: Box<str>,
    pub name: Box<str>,
    pub description: Box<str>,
    pub units: NonZeroU64,
}
