use serde::Deserialize;
use std::num::NonZeroU64;

use super::{parse_prefixed_enum_w_level, LeveledSymbol};
use crate::{PrefixedEnum, ShipComponentRequirements};

#[derive(Debug, Deserialize)]
#[serde(deny_unknown_fields)]
pub struct ShipEngine {
    #[serde(deserialize_with = "parse_prefixed_enum_w_level")]
    pub symbol: LeveledSymbol<ShipEngineSymbol>,
    pub name: Box<str>,
    pub description: Box<str>,
    pub condition: u64,
    pub integrity: u64,
    pub speed: NonZeroU64,
    pub requirements: ShipComponentRequirements,
}

#[allow(clippy::enum_variant_names)]
#[derive(Debug, Deserialize)]
#[serde(rename_all = "SCREAMING_SNAKE_CASE")]
pub enum ShipEngineSymbol {
    ImpulseDrive,
    IonDrive,
    HyperDrive,
}
impl PrefixedEnum for ShipEngineSymbol {
    const PREFIX: &'static str = "ENGINE_";
}
