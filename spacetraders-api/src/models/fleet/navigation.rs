use serde::Deserialize;

use crate::{System, SystemWaypoint, Timestamp, WaypointType};

#[derive(Debug, Deserialize)]
#[serde(rename_all = "camelCase", deny_unknown_fields)]
pub struct ShipNav {
    pub system_symbol: Box<str>,
    pub waypoint_symbol: Box<str>,
    pub route: ShipNavRoute,
    pub status: ShipNavStatus,
    pub flight_mode: ShipNavFlightMode,
}

#[derive(Debug, Deserialize)]
#[serde(deny_unknown_fields)]
pub struct ShipNavRoute {
    pub destination: ShipNavRouteWaypoint,
    pub origin: ShipNavRouteWaypoint,
    #[serde(rename = "departureTime")]
    pub depart_time: Timestamp,
    #[serde(rename = "arrival")]
    pub arrival_time: Timestamp,
}

#[derive(Debug, Deserialize)]
#[serde(rename_all = "camelCase", deny_unknown_fields)]
pub struct ShipNavRouteWaypoint {
    symbol: Box<str>,
    #[serde(rename = "type")]
    type_: WaypointType,
    system_symbol: Box<str>,
    x: i64,
    y: i64,
}

#[derive(Debug, Deserialize)]
#[serde(rename_all = "SCREAMING_SNAKE_CASE")]
pub enum ShipNavStatus {
    InTransit,
    InOrbit,
    Docked,
}

#[derive(Debug, Deserialize)]
#[serde(rename_all = "SCREAMING_SNAKE_CASE")]
pub enum ShipNavFlightMode {
    Drift,
    Stealth,
    Cruise,
    Burn,
}
