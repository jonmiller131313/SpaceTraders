use serde::Deserialize;

use super::{parse_prefixed_enum_w_level, LeveledSymbol, ShipComponentRequirements};
use crate::PrefixedEnum;

#[derive(Debug, Deserialize)]
#[serde(deny_unknown_fields)]
pub struct ShipMount {
    #[serde(deserialize_with = "parse_prefixed_enum_w_level")]
    pub symbol: LeveledSymbol<ShipMountSymbol>,
    pub name: Box<str>,
    pub description: Option<Box<str>>,
    pub strength: Option<u64>,
    pub deposits: Option<Box<[ShipMountDeposit]>>,
    pub requirements: ShipComponentRequirements,
}

#[derive(Debug, Deserialize)]
#[serde(rename_all = "SCREAMING_SNAKE_CASE")]
pub enum ShipMountSymbol {
    GasSiphon,
    Surveyor,
    SensorArray,
    MiningLaser,
    LaserCannon,
    MissileLauncher,
    Turret,
}
impl PrefixedEnum for ShipMountSymbol {
    const PREFIX: &'static str = "MOUNT_";
}

#[derive(Debug, Deserialize)]
#[serde(rename_all = "SCREAMING_SNAKE_CASE")]
pub enum ShipMountDeposit {
    QuartzSand,
    SiliconCrystals,
    PreciousStones,
    IceWater,
    AmmoniaIce,
    IronOre,
    CopperOre,
    SilverOre,
    AluminumOre,
    GoldOre,
    PlatinumOre,
    Diamonds,
    UraniteOre,
    MeritiumOre,
}
