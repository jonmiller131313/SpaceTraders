use serde::Deserialize;

use crate::Timestamp;

#[derive(Debug, Deserialize)]
#[serde(deny_unknown_fields)]
pub struct ShipFuel {
    pub current: u64,
    pub capacity: u64,
    pub consumed: ShipFuelConsumed,
}

#[derive(Debug, Deserialize)]
#[serde(deny_unknown_fields)]
pub struct ShipFuelConsumed {
    pub amount: u64,
    pub timestamp: Timestamp,
}
