use crate::PrefixedEnum;

use super::{parse_prefixed_enum_w_level, LeveledSymbol, ShipComponentRequirements};
use serde::Deserialize;

#[derive(Debug, Deserialize)]
#[serde(deny_unknown_fields)]
pub struct ShipModule {
    #[serde(deserialize_with = "parse_prefixed_enum_w_level")]
    pub symbol: LeveledSymbol<ShipModuleSymbol>,
    pub capacity: Option<u64>,
    pub range: Option<u64>,
    pub name: Box<str>,
    pub description: Box<str>,
    pub requirements: ShipComponentRequirements,
}

#[derive(Debug, Deserialize)]
#[serde(rename_all = "SCREAMING_SNAKE_CASE")]
pub enum ShipModuleSymbol {
    MineralProcessor,
    GasProcessor,
    CargoHold,
    CrewQuarters,
    EnvoyQuarters,
    PassengerCabin,
    MicroRefinery,
    OreRefinery,
    FuelRefinery,
    ScienceLab,
    JumpDrive,
    WarpDrive,
    ShieldGenerator,
}
impl PrefixedEnum for ShipModuleSymbol {
    const PREFIX: &'static str = "MODULE_";
}
