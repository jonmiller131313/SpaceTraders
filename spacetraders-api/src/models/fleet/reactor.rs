use serde::Deserialize;
use std::num::NonZeroU64;

use super::{parse_prefixed_enum_w_level, LeveledSymbol, ShipComponentRequirements};
use crate::PrefixedEnum;

#[derive(Debug, Deserialize)]
#[serde(rename_all = "camelCase", deny_unknown_fields)]
pub struct ShipReactor {
    #[serde(deserialize_with = "parse_prefixed_enum_w_level")]
    pub symbol: LeveledSymbol<ShipReactorSymbol>,
    pub name: Box<str>,
    pub description: Box<str>,
    pub condition: f64,
    pub integrity: f64,
    pub power_output: NonZeroU64,
    pub requirements: ShipComponentRequirements,
}

#[derive(Debug, Deserialize)]
#[serde(rename_all = "UPPERCASE")]
pub enum ShipReactorSymbol {
    Solar,
    Fusion,
    Fission,
    Chemical,
    Antimatter,
}
impl PrefixedEnum for ShipReactorSymbol {
    const PREFIX: &'static str = "REACTOR_";
}
