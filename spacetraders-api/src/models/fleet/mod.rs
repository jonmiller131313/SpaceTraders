use numerals::roman::Roman;
use serde::{de, Deserialize};
use serde_json::Value as Json;
use std::num::NonZeroU64;

mod crew;
mod engine;
mod frame;
mod fuel;
mod inventory;
mod module;
mod mount;
mod navigation;
mod reactor;

pub use self::{crew::*, engine::*, frame::*, fuel::*, inventory::*, module::*, mount::*, navigation::*, reactor::*};
use crate::{parse_prefixed_enum, FactionSymbol, PrefixedEnum, Timestamp};

#[derive(Debug, Deserialize)]
#[serde(deny_unknown_fields)]
pub struct Ship {
    pub symbol: Box<str>,
    pub registration: ShipRegistration,
    pub nav: ShipNav,
    pub crew: ShipCrew,
    pub frame: ShipFrame,
    pub reactor: ShipReactor,
    pub engine: ShipEngine,
    pub cooldown: ShipCooldown,
    pub modules: Box<[ShipModule]>,
    pub mounts: Box<[ShipMount]>,
    pub cargo: ShipCargo,
    pub fuel: ShipFuel,
}

#[derive(Debug, Deserialize)]
#[serde(rename_all = "camelCase", deny_unknown_fields)]
pub struct ShipRegistration {
    pub name: Box<str>,
    pub faction_symbol: FactionSymbol,
    pub role: ShipRole,
}

#[derive(Debug, Deserialize)]
#[serde(rename_all = "UPPERCASE")]
pub enum ShipRole {
    Fabricator,
    Harvester,
    Hauler,
    Interceptor,
    Excavator,
    Transport,
    Repair,
    Surveyor,
    Command,
    Carrier,
    Patrol,
    Satellite,
    Explorer,
    Refinery,
}

#[derive(Debug, Deserialize)]
#[serde(deny_unknown_fields)]
pub struct ShipComponentRequirements {
    pub power: Option<u64>,
    pub crew: Option<u64>,
    pub slots: Option<u64>,
}

#[derive(Debug, Deserialize)]
#[serde(rename_all = "camelCase", deny_unknown_fields)]
pub struct ShipCooldown {
    pub ship_symbol: Box<str>,
    pub total_seconds: u64,
    pub remaining_seconds: u64,
    pub expiration: Option<Timestamp>,
}

#[derive(Debug, Deserialize)]
#[serde(deny_unknown_fields)]
pub struct LeveledSymbol<S: PrefixedEnum> {
    pub symbol: S,
    pub level: u8,
}
fn parse_prefixed_enum_w_level<'de, M, D>(raw: D) -> Result<LeveledSymbol<M>, D::Error>
where
    M: de::DeserializeOwned + PrefixedEnum,
    D: de::Deserializer<'de>,
{
    let raw: &'de str = de::Deserialize::deserialize(raw)?;
    let (prefixed, level) = raw
        .rsplit_once('_')
        .ok_or_else(|| de::Error::custom(format!("enum does not have a version: {raw}")))?;

    let symbol = parse_prefixed_enum(Json::from(prefixed)).map_err(de::Error::custom)?;

    let level = Roman::parse(level)
        .ok_or_else(|| de::Error::custom(format!("enum version is not valid roman numeral: {level}")))?
        .value() as _;

    Ok(LeveledSymbol { symbol, level })
}
