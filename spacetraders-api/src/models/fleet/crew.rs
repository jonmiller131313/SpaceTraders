use serde::Deserialize;

#[derive(Debug, Deserialize)]
#[serde(deny_unknown_fields)]
pub struct ShipCrew {
    pub current: u64,
    pub required: u64,
    pub capacity: u64,
    pub rotation: ShipCrewRotation,
    pub morale: u8,
    pub wages: u64,
}

#[derive(Debug, Deserialize)]
#[serde(rename_all = "SCREAMING_SNAKE_CASE")]
pub enum ShipCrewRotation {
    Strict,
    Relaxed,
}
