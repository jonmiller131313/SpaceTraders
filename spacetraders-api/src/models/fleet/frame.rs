use serde::{Deserialize, Deserializer};

use super::ShipComponentRequirements;
use crate::{parse_prefixed_enum, PrefixedEnum};

#[derive(Debug, Deserialize)]
#[serde(rename_all = "camelCase", deny_unknown_fields)]
pub struct ShipFrame {
    #[serde(deserialize_with = "parse_prefixed_enum")]
    pub symbol: ShipFrameSymbol,
    pub name: Box<str>,
    pub description: Box<str>,
    pub condition: f64,
    pub integrity: f64,
    pub module_slots: u64,
    pub mounting_points: u64,
    pub fuel_capacity: u64,
    pub requirements: ShipComponentRequirements,
}

#[derive(Debug, Deserialize, PartialEq)]
#[serde(rename_all = "SCREAMING_SNAKE_CASE")]
pub enum ShipFrameSymbol {
    Probe,
    Drone,
    Interceptor,
    Racer,
    Fighter,
    Frigate,
    Shuttle,
    Explorer,
    Miner,
    LightFreighter,
    HeavyFreighter,
    Transport,
    Destroyer,
    Cruiser,
    Carrier,
}
impl PrefixedEnum for ShipFrameSymbol {
    const PREFIX: &'static str = "FRAME_";
}
