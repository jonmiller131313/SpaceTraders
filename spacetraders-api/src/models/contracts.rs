use serde::Deserialize;

use crate::{prelude::Timestamp, FactionSymbol};

#[derive(Debug, Deserialize)]
#[serde(rename_all = "camelCase", deny_unknown_fields)]
pub struct Contract {
    pub id: Box<str>,
    pub faction_symbol: FactionSymbol,
    #[serde(rename = "type")]
    pub type_: ContractType,
    pub terms: ContractTerms,
    pub accepted: bool,
    pub fulfilled: bool,
    pub expiration: Timestamp,
    pub deadline_to_accept: Timestamp,
}

#[derive(Debug, Deserialize)]
#[serde(rename_all = "UPPERCASE")]
pub enum ContractType {
    Procurement,
    Transport,
    Shuttle,
}

#[derive(Debug, Deserialize)]
#[serde(deny_unknown_fields)]
pub struct ContractTerms {
    pub deadline: Timestamp,
    pub payment: ContractTermsPayment,
    pub deliver: Box<[ContractTermsDeliver]>,
}

#[derive(Debug, Deserialize)]
#[serde(rename_all = "camelCase", deny_unknown_fields)]
pub struct ContractTermsPayment {
    pub on_accepted: u64,
    pub on_fulfilled: u64,
}

#[derive(Debug, Deserialize)]
#[serde(rename_all = "camelCase", deny_unknown_fields)]
pub struct ContractTermsDeliver {
    pub trade_symbol: Box<str>,
    pub destination_symbol: Box<str>,
    pub units_required: u64,
    pub units_fulfilled: u64,
}
