use serde::Deserialize;

use crate::FactionSymbol;

#[derive(Debug, Deserialize)]
#[serde(rename_all = "camelCase", deny_unknown_fields)]
pub struct Agent {
    pub account_id: Option<Box<str>>,
    pub symbol: Box<str>,
    pub headquarters: Box<str>,
    pub credits: i64,
    pub starting_faction: FactionSymbol,
    pub ship_count: u64,
}
