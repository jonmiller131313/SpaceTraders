use std::borrow::Cow;

use serde::{de, Deserialize};

pub trait PrefixedEnum {
    const PREFIX: &'static str;
}
pub(crate) fn parse_prefixed_enum<'de, M, D>(raw: D) -> Result<M, D::Error>
where
    M: de::DeserializeOwned + PrefixedEnum,
    D: de::Deserializer<'de>,
{
    let raw: Cow<str> =
        de::Deserialize::deserialize(raw).map_err(|e| de::Error::custom(format!("unable to deserialize: {e}")))?;
    raw.strip_prefix(M::PREFIX)
        .ok_or_else(|| format!(r#"enum does not start with prefix "{}": {raw}"#, M::PREFIX))
        .and_then(|symbol| serde_json::from_value(symbol.into()).map_err(|e| format!(r#"invalid enum ("{raw}"): {e}"#)))
        .map_err(de::Error::custom)
}

pub(crate) fn parse_json<T: de::DeserializeOwned>(json: impl AsRef<[u8]>) -> T {
    serde_json::from_slice(json.as_ref()).unwrap_or_else(|e| invalid_json_bail(e, json))
}

pub(super) fn invalid_json_bail(e: serde_json::Error, bytes: impl AsRef<[u8]>) -> ! {
    use std::{env::temp_dir, fs, io::Error, str};

    use chrono::Local;

    let bytes = bytes.as_ref();
    if let Ok(s) = str::from_utf8(bytes) {
        panic!("Unable to parse response JSON: {e}\n{s:#}");
    }
    // Else (try to) write to file

    let dump_fail = |io_e: Error| -> ! {
        panic!("Unable to parse response JSON: {e}\nFailed to dump file: {io_e:?}");
    };

    let dump_file = temp_dir().join(format!("spacetraders/dump_{}", Local::now().format("%F_%T")));
    let _ = fs::create_dir_all(dump_file.parent().unwrap()).map_err(dump_fail);
    let _ = fs::write(&dump_file, bytes).map_err(dump_fail);

    panic!(
        "Unable to parse response JSON: {e}\nData written to: {}",
        dump_file.display()
    )
}
