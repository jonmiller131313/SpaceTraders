use std::{borrow::Cow, future::Future};

use bytes::Bytes;
use chrono::{DateTime, Utc};
use once_cell::sync::Lazy;
use reqwest::{
    header::{HeaderMap, HeaderName},
    Client, Method,
};
use serde::{de::DeserializeOwned, Deserialize, Serialize};
use serde_json::Value as Json;
use url::Url;

use crate::error::ApiError;

mod de;
mod pagination;

pub use self::{de::*, pagination::*};

pub type Timestamp = DateTime<Utc>;
pub type Range = std::ops::Range<u64>;
pub type Result<T, E = ApiError> = std::result::Result<T, E>;

pub trait Endpoint {
    fn method(&self) -> Method {
        Method::GET
    }

    fn endpoint(&self) -> impl Into<Cow<'static, str>>;

    fn parameters(&self) -> &[(&str, Cow<'static, str>)] {
        &[]
    }

    fn body(&self) -> Option<Json> {
        None
    }
}

#[repr(transparent)]
#[derive(Clone, Debug)]
pub struct SpaceTradersClient(Client);
impl SpaceTradersClient {
    pub fn new_no_auth() -> Self {
        Self::new(None).unwrap()
    }

    /// Returns None if the token cannot be represented in an HTTP header.
    pub fn new(token: impl Into<Option<String>>) -> Option<Self> {
        let builder = Client::builder();

        let mut headers = HeaderMap::new();
        for header in ["content-type", "accept"] {
            headers.insert(header, "application/json".try_into().expect("Hard-coded value"));
        }
        if let Some(token) = token.into() {
            headers.insert("authorization", format!("Bearer {token}").try_into().ok()?);
        }

        Some(Self(builder.default_headers(headers).build().ok()?))
    }

    async fn request<T: Serialize + ?Sized>(
        &self,
        endpoint: &str,
        method: Method,
        params: &T,
        body: impl Into<Option<Json>>,
    ) -> Result<Bytes> {
        static URL: Lazy<Url> = Lazy::new(|| Url::parse("https://api.spacetraders.io/v2/").expect("Hardcoded URL"));

        // Joining URL's follow similar rules for Path's ("absolute" urls that start with '/' overwrite the "v2" in the URL).
        let url = URL
            .join(endpoint.strip_prefix('/').unwrap_or(endpoint))
            .unwrap_or_else(|e| panic!("Invalid endpoint '{endpoint}': {e}"));

        let mut request = self.0.request(method, url).query(params);

        if let Some(body) = body.into() {
            request = request.json(&body);
        }

        let res = request.send().await.map_err(ApiError::Network)?;

        if res.error_for_status_ref().is_ok() {
            res.bytes().await.map_err(ApiError::Network)
        } else {
            let bytes = res.bytes().await.map_err(ApiError::Network)?;
            Err(ApiError::server(bytes))
        }
    }
}

pub(crate) trait Query<T> {
    fn query(&self, client: &SpaceTradersClient) -> impl Future<Output = Result<T>>;
}

impl<E, T> Query<T> for E
where
    E: Endpoint,
    T: DeserializeOwned,
{
    async fn query(&self, client: &SpaceTradersClient) -> Result<T> {
        let bytes = client
            .request(&self.endpoint().into(), self.method(), self.parameters(), self.body())
            .await?;

        Ok(parse_json(bytes))
    }
}

pub struct Ignore<E>(E);
pub fn ignore<E>(e: E) -> Ignore<E> {
    Ignore(e)
}
impl<E: Endpoint> Query<()> for Ignore<E> {
    async fn query(&self, client: &SpaceTradersClient) -> Result<()> {
        client
            .request(
                &self.0.endpoint().into(),
                self.0.method(),
                self.0.parameters(),
                self.0.body(),
            )
            .await?;
        Ok(())
    }
}

pub struct Raw<E>(E);
pub fn raw<E>(e: E) -> Raw<E> {
    Raw(e)
}
impl<E: Endpoint> Query<Vec<u8>> for Raw<E> {
    async fn query(&self, client: &SpaceTradersClient) -> Result<Vec<u8>> {
        Ok(client
            .request(
                &self.0.endpoint().into(),
                self.0.method(),
                self.0.parameters(),
                self.0.body(),
            )
            .await?
            .into())
    }
}

pub(crate) async fn query_single<E, T>(endpoint: &E, client: &SpaceTradersClient) -> Result<Box<T>>
where
    E: Endpoint,
    T: DeserializeOwned,
{
    #[derive(Deserialize)]
    struct ResponseData<E> {
        data: Box<E>,
    }

    let response: ResponseData<T> = endpoint.query(client).await?;
    Ok(response.data)
}

#[cfg(test)]
pub(crate) fn get_client() -> Option<SpaceTradersClient> {
    const TOKEN: Option<&'static str> = None;

    TOKEN.map(|token| SpaceTradersClient::new(token.to_string()).unwrap())
}
