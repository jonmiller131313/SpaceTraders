use std::num::{NonZeroU64, NonZeroU8};

use crate::Agent;
use serde::{de::DeserializeOwned, Deserialize};
use serde_json::value::{RawValue as RawJson, RawValue};

use crate::prelude::*;

pub trait Pageable {}

pub(crate) async fn query_range<E, T>(
    endpoint: E,
    client: &SpaceTradersClient,
    range: impl Into<Option<Range>>,
) -> Result<Box<[T]>>
where
    E: Endpoint + Pageable,
    T: DeserializeOwned,
{
    let e = paged(endpoint, range.into().map(Pagination::Range).unwrap_or(Pagination::All));
    e.query(client).await
}

#[derive(Deserialize)]
struct PagedData {
    data: Box<RawJson>,
    meta: PaginationData,
}

#[derive(Deserialize)]
struct PaginationData {
    total: u64,
    page: NonZeroU64,
    limit: NonZeroU8,
}

pub enum Pagination {
    All,
    Range(Range),
}
impl From<Range> for Pagination {
    fn from(range: Range) -> Self {
        Self::Range(range)
    }
}

pub(crate) fn paged<E>(endpoint: E, pagination: Pagination) -> Paged<E> {
    Paged { endpoint, pagination }
}

pub struct Paged<E> {
    endpoint: E,
    pagination: Pagination,
}
impl<E, T> Query<Box<[T]>> for Paged<E>
where
    E: Endpoint + Pageable,
    T: DeserializeOwned,
{
    async fn query(&self, client: &SpaceTradersClient) -> Result<Box<[T]>> {
        const LIMIT: u64 = 20;

        let mut buf = Vec::new();
        let mut page_index = match self.pagination {
            Pagination::All => 1,
            Pagination::Range(Range { start, .. }) => start / LIMIT + 1,
        };

        let method = self.endpoint.method();
        let endpoint = self.endpoint.endpoint().into();
        let body = self.endpoint.body();

        let mut params = Vec::from(self.endpoint.parameters());
        params.extend([
            ("limit", LIMIT.to_string().into()),
            ("page", page_index.to_string().into()),
        ]);

        loop {
            params.last_mut().unwrap().1 = page_index.to_string().into();

            let bytes = client.request(&endpoint, method.clone(), &params, body.clone()).await?;

            let PagedData {
                data,
                meta: PaginationData { total, .. },
            } = parse_json(bytes);

            let mut data: Vec<T> = parse_json(data.get());

            match self.pagination {
                Pagination::All => {
                    buf.extend(data);

                    if total <= LIMIT * page_index {
                        break;
                    }
                }

                Pagination::Range(Range { start, end }) => {
                    let (page_start_i, page_end_i) = (LIMIT * (page_index - 1), LIMIT * page_index);

                    let start_i = if start <= page_start_i { 0 } else { start - page_start_i };

                    let end_i = if end >= page_end_i {
                        data.len() as u64
                    } else {
                        end - page_start_i
                    };

                    buf.extend(data.drain(start_i as usize..end_i as usize));

                    if page_end_i >= end {
                        break;
                    }
                }
            }
            page_index += 1;
        }

        Ok(buf.into_boxed_slice())
    }
}

pub(crate) async fn query_len<E>(endpoint: &E, client: &SpaceTradersClient) -> Result<u64>
where
    E: Endpoint + Pageable + ?Sized,
{
    let mut parameters = Vec::from(endpoint.parameters());
    parameters.push(("limit", "1".into()));

    let bytes = client
        .request(
            &endpoint.endpoint().into(),
            endpoint.method(),
            &parameters,
            endpoint.body(),
        )
        .await?;
    let PagedData {
        data: _,
        meta: PaginationData { total, .. },
    } = parse_json(bytes);
    Ok(total)
}
