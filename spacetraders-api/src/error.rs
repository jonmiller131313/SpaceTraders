use std::fmt;

use bytes::Bytes;
use reqwest::Error;
use serde::Deserialize;
use serde_json::Value as Json;
use thiserror::Error;

use crate::prelude::parse_json;

#[derive(Debug, Error)]
pub enum ApiError {
    #[error("symbol must be between 3 and 14 characters: {0}")]
    InvalidSymbol(Box<str>),

    #[error("network error")]
    Network(#[from] Error),

    #[error("{0}")]
    Server(ServerError),
}
impl ApiError {
    pub(crate) fn server(json: Bytes) -> Self {
        #[derive(Deserialize)]
        struct OuterServerError {
            error: ServerError_,
        }

        #[derive(Deserialize)]
        struct ServerError_ {
            code: u16,
            message: Box<str>,
            data: ServerErrorPayload,
        }
        #[derive(Deserialize)]
        struct ServerErrorPayload {
            symbol: [Box<str>; 1],
        }

        let ServerError_ {
            code,
            message: title,
            data: ServerErrorPayload { symbol: [description] },
        } = parse_json::<OuterServerError>(json).error;

        Self::Server(ServerError {
            code,
            title,
            description,
        })
    }
}

#[derive(Debug)]
pub struct ServerError {
    pub code: u16,
    pub title: Box<str>,
    pub description: Box<str>,
}
impl fmt::Display for ServerError {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "[server code: {}] {} {}", self.code, self.title, self.description)
    }
}
